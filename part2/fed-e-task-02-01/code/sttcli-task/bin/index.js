#!/usr/bin/env node

// 脚手架工作过程：
// 1.通过命令行交互询问用户问题
// 2.根据用户回答的结果生成文件
const path = require('path')
//用户命令与交互行工具
const inquirer = require('inquirer')
//文件系统模块(fs)
const fs = require('fs')
// 模板引擎
const ejs = require('ejs')
inquirer.prompt([{
        type: 'input',
        name: 'name',
        message: 'Your project name?',
        default: 'stt-tasks',
    },
    {
        type: 'input',
        name: 'descriptions',
        message: 'Your project descriptions?',
        default: 'stt-tasks',
    },
    {
        type: 'input',
        message: 'Your phonenumber:',
        name: 'phone',
        validate: function (input) {
            var done = this.async(); // 进行异步操作 
            setTimeout(function () {
                if (input.match(/\d{11}/g)) {
                    done(null, true)
                }
                done('请输入11位数字')
                return
            }, 1000)
        }
    },
]).then(answers => {
    //模板目录
    const tempPath = path.join(process.cwd(),'templates')

    // 用于渲染目录的模板文件  files是数组
    fs.readdir(tempPath,(err,files)=>{
        if (err) throw err
        // (文件路径，参数，回调函数)
        files.forEach(item=>{
            ejs.renderFile(path.join(tempPath,item),answers,(err,res)=>{
                if (err) throw err
                console.log(res)
                fs.writeFileSync(path.join(process.cwd(),item),res)
            })
        })
        
    })

})