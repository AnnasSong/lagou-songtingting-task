## 编程题

**1、概述脚手架实现的过程，并使用 NodeJS 完成一个自定义的小型脚手架工具**
1.创建脚手架文件夹，yarn init初始化一个package.json
2.package.json文件配置bin字段，配置脚手架入口文件，入口文件头要配置#!/usr/bin/env node
3.yarn link将脚手架link到全局，执行脚手架命令检查是否成功
4.安装inquirer（用户命令交互行工具）
    inquirer.prompt(questions).then(answers=>{})
5.fs(文件系统模块)读取模板文件，进行其他文件操作后，写入目标文件

# pages-boilerplate

> Always a pleasure scaffolding your awesome static sites.

## Getting Started

```shell
# clone repo
$ git clone https://gitee.com/AnnasSong/lagou-songtingting-task.git sttcli-task
$ cd sttcli-task
# install dependencies
$ yarn # or npm install
```

## Usage

```shell
$ yarn <task> [options]
```

### e.g.

```shell
# Runs the app in development mode
$ yarn serve 
# Builds the app for production to the `dist` folder
$ yarn build 
```

#### `yarn sttcli-task` or `npm run sttcli-task`

Compile the styles & scripts & pages file.

## Folder Structure

```
└─sttcli-task ································· project root
    │  index.html ······························· page file 
    │  package.json ································· package file
    │  README.md ···································· repo readme
    │  yarn.lock ···································· yarn lock file
    │  
    ├─bin ·································· node tasks folder
    │      index.js ·································· tasks file
    │      
    └─templates ······························ templates folder
            index.html ······························ page file
```

