# pages-boilerplate

> Always a pleasure scaffolding your awesome static sites.

## Getting Started

```shell
# clone repo
$ git clone https://gitee.com/AnnasSong/lagou-songtingting-task.git pages-boilerplate
$ cd pages-boilerplate
# install dependencies
$ yarn # or npm install
```

## Usage

```shell
$ yarn <task> [options]
```

### e.g.

```shell
# Runs the app in development mode
$ yarn serve 
# Builds the app for production to the `dist` folder
$ yarn build 
```

#### `yarn compile` or `npm run compile`

Compile the styles & scripts & pages file.

#### `yarn serve` or `npm run serve`

Runs the app in development mode with a automated server.

#### `yarn clean` or `npm run clean`

Clean the `dist` & `temp` files.

## Folder Structure

```

└─pages-boilerplate ································· project root
    │  .editorconfig ································ editor config file
    │  .gitignore ··································· git ignore file
    │  .npmrc ······································· registry link
    │  .travis.yml ·································· travis ci config file
    │  gulpfile.js ·································· gulp tasks file
    │  LICENSE ······································ repo license
    │  package.json ································· package file
    │  README.md ···································· repo readme
    │  yarn.lock ···································· yarn lock file
    │  
    ├─public ········································ static folder
    │      favicon.ico ······························ static file (unprocessed)
    │      
    └─src ··········································· source folder
        │  about.html ······························· page file (use layout & partials)
        │  index.html ······························· page file (use layout & partials)
        │  
        ├─assets ···································· assets folder
        │  ├─fonts ·································· fonts folder
        │  │      pages.eot ························· font file (imagemin)
        │  │      pages.svg ························· font file (imagemin)
        │  │      pages.ttf ························· font file (imagemin)
        │  │      pages.woff ························ font file (imagemin)
        │  │      
        │  ├─images ································· images folder
        │  │      brands.svg ························ image file (imagemin)
        │  │      logo.png ·························· image file (imagemin)
        │  │      
        │  ├─scripts ·································· scripts folder
        │  │      main.js ······························· script file (babel / uglify)
        │  │      
        │  └─styles ··································· styles folder
        │          main.scss ····························· entry scss file (scss / postcss)
        │          _icons.scss······················· partial sass file (dont output)
        │          _variables.scss······················· partial sass file (dont output)
        │          
        ├─layouts ····································· layouts folder
        │      basic.html ······························· layout file (dont output)
        │      
        └─partials ···································· partials folder
                footer.html ······························ partial file (dont output)
                header.html ······························ partial file (dont output)
```

