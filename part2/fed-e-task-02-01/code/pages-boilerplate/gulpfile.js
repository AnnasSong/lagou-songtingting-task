// 实现这个项目的构建任务
const {
  src,
  dest,
  parallel,
  series,
  watch
} = require('gulp')
const Plugins = require('gulp-load-plugins')()
const del = require('del')
const bs = require('browser-sync').create()
const data = {
  menus: [{
          name: 'Home',
          icon: 'aperture',
          link: 'index.html'
      },
      {
          name: 'Features',
          link: 'features.html'
      },
      {
          name: 'About',
          link: 'about.html'
      },
      {
          name: 'Contact',
          link: '#',
          children: [{
                  name: 'Twitter',
                  link: 'https://twitter.com/w_zce'
              },
              {
                  name: 'About',
                  link: 'https://weibo.com/zceme'
              },
              {
                  name: 'divider'
              },
              {
                  name: 'About',
                  link: 'https://github.com/zce'
              }
          ]
      }
  ],
  pkg: require('./package.json'),
  date: new Date()
}
// css文件编译
const styles = ()=>{
    return src('src/assets/styles/*.scss',{base:'src'})
    .pipe(Plugins.sass())
    .pipe(dest('.temp'))
    .pipe(bs.reload({stream: true}))
}
//js文件编译
const scripts = ()=>{
  return src('src/assets/scripts/*.js',{base:'src'})
  .pipe(Plugins.babel({
    presets:['@babel/preset-env']
  }))
  .pipe(dest('.temp'))
  .pipe(bs.reload({stream: true}))
}
//html文件编译
const pages = ()=>{
  return src('src/*.html',{base:'src'})
  .pipe(Plugins.swig({data,defaults: { cache: false }}))
  .pipe(dest('.temp'))
  .pipe(bs.reload({stream: true}))
}
//图片打包
const images = ()=>{
  return src('src/assets/images/**',{base:'src'})
  .pipe(Plugins.imagemin())
  .pipe(dest('dist'))
}
//font文件打包
const fonts = ()=>{
  return src('src/assets/fonts/**',{base:'src'})
  .pipe(Plugins.imagemin())
  .pipe(dest('dist'))
}
//其他文件打包
const extra = ()=>{
  return src('public/**',{base:'public'})
  .pipe(dest('dist'))
}

//清除dist文件
const clean = ()=>{
  return del(['dist','.temp'])
}
// http://www.browsersync.cn/docs/options/#option-server
const serve = ()=>{
  //监听文件修改自动编译
  watch('src/assets/styles/*.scss',{base:'src'},styles)
  watch('src/assets/scripts/*.js',{base:'src'},scripts)
  watch('src/*.html',{base:'src'},pages)
  //其他文件更新只需要reload服务器，不需要打包
  watch(['src/assets/images/**','src/assets/fonts/**'],{base:'src'},bs.reload({stream: true}))
  watch('public/**',{base:'public'},{base:'public'},bs.reload({stream: true}))

  bs.init({
    // 监听文件修改热更新
    // files:'dist/**',
    server:{
      port:2080,
      baseDir:'.temp',
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}
// gulp-useref文件引用处理,将html引用的多个css和js文件合并起来，只负责合并不负责压缩
// 如果要压缩或执行其他一些修改，则可以使用 gulp-if 有条件地处理特定类型的资产
const useref = ()=>{
  return src('.temp/*.html',{base:'.temp'})
  .pipe(Plugins.useref({
    // 文件搜索路径
    searchPath: ['.temp','.']
  }))
  // js css html 文件压缩
  .pipe(Plugins.if(/\.js$/,Plugins.uglify()))
  .pipe(Plugins.if(/\.css$/,Plugins.cleanCss()))
  .pipe(Plugins.if(/\.html$/,Plugins.htmlmin({
    collapseWhitespace:true,
    minifyCSS:true,
    minifyJS:true
  })))
  .pipe(dest('dist'))
}

const compile = parallel(styles,scripts,pages)
const build = series(clean,parallel(series(compile,useref),images,fonts,extra))
const develop = series(compile,serve)
module.exports = {
  clean,
  build,
  develop,
  compile
}
