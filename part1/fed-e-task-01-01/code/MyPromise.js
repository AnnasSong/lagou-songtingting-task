/*
尽可能还原 Promise 中的每一个 API, 并通过注释的方式描述思路和原理.
*/

const { reject, add } = require("lodash")

/*
    1.Promise就是一个类，在执行这个类的时候，需要传递一个执行器进去，执行器会立即执行
    2.Promise中有三种状态，分别为fullfilled rejected pending
        pending=> fullfilled
        pending=> rejected
        一旦状态确定就不可更改
    3.resolve和rejecte函数是用来更改状态的
    4.then方法内部是判断状态成功就执行成功回调，失败就执行失败回调
    5.then 成功回调方法有一个参数，代表成功后的值  then失败回调方法有一个参数，代表失败后的值
    6.then方法多次调用
*/
const PENDING = 'pending'
const FULLFILLED = 'fullfilled'
const REJECTED = 'rejected'
class MyPromise {
    constructor(executor){
        try{
            executor(this.resolve,this.reject)
        }catch(e){
            this.reject(e)
        }
    }
    status = PENDING
    value = undefined
    reason = undefined
    successCallback = []
    failCallback = []
    resolve = (value) => {
        // 状态只有为pending状态才可更改状态
        if(this.status!=PENDING) return 
        // 状态置为成功
        this.status = FULLFILLED
        this.value = value
        // 判断成功回调是否存在，存在则调用
        // this.successCallback&&this.successCallback(this.value)
        // 回调函数依次取出调用
        while(this.successCallback.length) this.successCallback.shift()()
    }
    reject = (reason) => {
        // 状态只有为pending状态才可更改状态
        if(this.status!=PENDING) return 
        // 状态置为失败
        this.status = REJECTED
        this.reason = reason
        // 判断失败回调是否存在，存在则调用
        // this.failCallback&&this.failCallback(this.reason)
        // 回调函数依次取出调用
        while(this.failCallback.length) this.failCallback.shift()()
    }
    then = (successCallback,failCallback)=>{
        successCallback = successCallback?successCallback:value=>value
        failCallback = failCallback?failCallback:reason=>{throw reason}
        let promise2 = new MyPromise((resolve,reject)=>{
             // 成功状态执行成功回调
            if( this.status === FULLFILLED){
                 // 由于在promise2创建过程中传递了promise2,导致报错,所以放在settimeout里面异步调用,
                setTimeout(()=>{
                    try{
                         // then链式调用,把上一个then的返回值保存,通过resolve传递,作为下一个then的参数
                        let x =  successCallback(this.value)
                        // 判断x的值是普通值还是promise对象
                        // 如果是普通值,直接调用resolve
                        // 如果是promise对象,需要判断promise对象的返回结果,
                        // 根据返回结果决定调用resolve,还是调用reject
                        resolvePromise(promise2,x,resolve,reject)
                    }catch(e){
                        reject(e)
                    }
                },0)
            }
            // 失败状态执行失败回调
            else if( this.status === REJECTED){
                // 由于在promise2创建过程中传递了promise2,导致报错,所以放在settimeout里面异步调用,
                setTimeout(()=>{
                    try{
                         // then链式调用,把上一个then的返回值保存,通过resolve传递,作为下一个then的参数
                        let x =  failCallback(this.reason)
                        // 判断x的值是普通值还是promise对象
                        // 如果是普通值,直接调用resolve
                        // 如果是promise对象,需要判断promise对象的返回结果,
                        // 根据返回结果决定调用resolve,还是调用reject
                        // 由于在promise2创建过程中传递了promise2,导致报错,所以放在settimeout里面异步调用,
                        resolvePromise(promise2,x,resolve,reject)
                    }catch(e){
                        reject(e)
                    }
                },0)
            }
            // 等待状态（处理异步）
            else{
                // then多次调用，异步情况，只会保存最后一次的回调函数，因此要把所有的回调函数保存起来然后依次调用
                // 将成功和失败回调函数存储起来
                // this.successCallback.push(successCallback)
                this.successCallback.push(()=>{
                    // 由于在promise2创建过程中传递了promise2,导致报错,所以放在settimeout里面异步调用,
                    setTimeout(()=>{
                        try{
                             // then链式调用,把上一个then的返回值保存,通过resolve传递,作为下一个then的参数
                            let x =  successCallback(this.value)
                            // 判断x的值是普通值还是promise对象
                            // 如果是普通值,直接调用resolve
                            // 如果是promise对象,需要判断promise对象的返回结果,
                            // 根据返回结果决定调用resolve,还是调用reject
                            resolvePromise(promise2,x,resolve,reject)
                        }catch(e){
                            reject(e)
                        }
                    },0)
                })
                this.failCallback.push(()=>{
                    // 由于在promise2创建过程中传递了promise2,导致报错,所以放在settimeout里面异步调用,
                    setTimeout(()=>{
                        try{
                             // then链式调用,把上一个then的返回值保存,通过resolve传递,作为下一个then的参数
                            let x =  failCallback(this.reason)
                            // 判断x的值是普通值还是promise对象
                            // 如果是普通值,直接调用resolve
                            // 如果是promise对象,需要判断promise对象的返回结果,
                            // 根据返回结果决定调用resolve,还是调用reject
                            resolvePromise(promise2,x,resolve,reject)
                        }catch(e){
                            reject(e)
                        }
                    },0)
                })
            }
        })
        return promise2
    }
    //Promise.all()传入一个数组,数组中每个元素都是一个promise对象，会等待所有promise执行完毕以后执行then，
    // then接收的参数是包含所有promise返回结果的数组
    static all(arr){
        let result = []
        let index = 0
        return new MyPromise((resolve,reject)=>{
            function addData(i,value){
                result[i] = value
                index++
                // 异步操作无法及时获得返回值,需要如下操作
                if(index===arr.length) resolve(result) 
            }
            // 遍历数组,依次处理放入结果数组中
            for(let i=0;i<arr.length;i++){
                let current = arr[i]
                if(current instanceof MyPromise){
                    current.then(res=>{
                        addData(i,res)
                    },err=>{
                        reject(err)
                    })
                }else{
                    // result.push(current) 不能直接push,push因为延迟原因,导致输出顺序不一致 [ 'a', 'b', 'c', 'promise2' ]
                    addData(i,current)
                }
            }
        })
    }
    // Promise.race()传入一个数组,只会等待第一个结束的任务
    static race(arr){
        return new MyPromise((resolve,reject)=>{
            // 遍历数组,依次处理放入结果数组中
            for(let i=0;i<arr.length;i++){
                let current = arr[i]
                if(current instanceof MyPromise){
                    current.then(res=>{
                        resolve(res)
                    },err=>{
                        reject(err)
                    })
                }else{
                    resolve(current) 
                }
            }
        })
    }
    static resolve(value){
        if(value instanceof MyPromise) return value
        return new MyPromise(resolve=>resolve(value))
    }
    // Promise.finally()无论当前的promise状态是成功还是失败,finally方法中的回调函数都会执行一次
    // 在finally方法后可以链式调用then方法,拿到当前promise对象返回值
    finally=(callback)=>{
        return this.then(value=>{
            // finally方法如果执行的是异步方法,则需要等待执行完毕以后再执行后续then操作
            return MyPromise.resolve(callback()).then(()=>value)
        },err=>{
            return MyPromise.resolve(callback()).then(()=>{throw err})
        })
    }
    catch=(callback)=>{
        return this.then(undefined,callback)
    }
}
function resolvePromise(promise2,x,resolve,reject){
    if(promise2===x){
        return reject(new Error('Chaining cycle detected for promise'))
    }
    if(x instanceof MyPromise){
    // promise对象
        // x.then(res=>{
        //     resolve(res)
        // },err=>{
        //     reject(err)
        // })  
        x.then(resolve,reject)         
    }else{
    // 普通值
        resolve(x)
    }
}



// let promise = new MyPromise((resolve,reject)=>{
//     // resolve('成功')
//     // reject('失败')
//     setTimeout(function(){
//         resolve('成功')
//         // reject('失败')
//     },1000)
//     // throw new Error('err')
// })
// let promise2 = new MyPromise((resolve,reject)=>{
//     resolve('promise2')
// })
// promise.then(res=>{
//     console.log(res)
//     return promise2
// },err=>{
//     console.log(err)
//     // return promise2
// }).then(res=>{
//     console.log(res)
// },err=>{
//     console.log(err)
// })
// var p1 = promise.then(res=>{
//     console.log(res)
//     return p1
// })
// p1.then(()=>{},err=>{
//     console.log(err.message)
// })

// promise.then().then().then(res=>{
//     console.log(res)
// },err=>{
//     console.log(err)
// })

let promise = new MyPromise((resolve,reject)=>{
    // resolve('成功')
    // reject('失败')
    setTimeout(function(){
        resolve('成功')
        // reject('失败')
    },2000)
    // throw new Error('err')
})
let promise2 = new MyPromise((resolve,reject)=>{
    
    setTimeout(function(){
        resolve('promise2')
        // reject('失败')
    },3000)
    // reject('promise2')
})
// MyPromise.all(['a','b',promise,promise2,'c']).then(res=>{
//     console.log(res)
// })
// MyPromise.resolve('a').then(res=>{
//     console.log(res)
// })
// promise2.finally(()=>{return promise}).then(res=>{
//     console.log(res)
// },err=>{
//     console.log(err)
// })

// promise2.then(res=>{
//     throw new Error('err')
// }).catch(err=>{
//     console.log(err.message)
// })
Promise.race([promise,promise2]).then(res=>{
    console.log(res)
})