/*
  将下面异步代码使用 Promise 的方法改进
  尽量用看上去像同步代码的方式
  setTimeout(function () {
    var a = 'hello'
    setTimeout(function () {
      var b = 'lagou'
      setTimeout(function () {
        var c = 'I ♥ U'
        console.log(a + b +c)
      }, 10)
    }, 10)
  }, 10)
*/

let handleTimeout = (newV,defV='') =>{
  return new Promise((resolve,reject)=>{
    setTimeout(function () {
      resolve(defV + ' ' + newV)
    }, 10)
  })
  
}

handleTimeout('hello').then(res=>{
  return handleTimeout('lagou',res)
}).then(res=>{
  return handleTimeout('I ♥ U',res)
}).then(res=>{
  console.log(res)
})